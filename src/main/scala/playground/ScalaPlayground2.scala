package playground

object ScalaPlayground2 {

  def main(args: Array[String]): Unit = {

    val x = null

    Option(x).foreach(x =>
      println(x)
    )

    //good way to handle null ..
    val y = 10

    Option(y).foreach(y =>
      println(y)
    )

    val aList = Seq(1, 2, 3, 4, 5)

    val filteredList = for(
      x <- aList if x > 4
    ) yield x

    println(filteredList)


    val doubler2 = (x: Int) => x * 2 //syntactic sugar for the above

    //same as above, specify type
    val double22: (Int => Int) = (x:Int) => x * 2
    //or
    val doubler3: (Int => Int) = x => x * 2


    val tuples = Seq((1, 2), (3, 4), (5, 6))

    val myFilter:(Int, Int) => Boolean = (x, y) => true

    //this works
    tuples.filter(item => item._1 > 1);

    //this doesn't work
    //tuples.filter(myFilter);


    val myList =  List(2, 2, 3, 4)

    myList match{
      case 1 :: tail => println("got it");
      case 2 :: tail => println("got it here");
    }



  }


}
