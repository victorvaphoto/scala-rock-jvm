package playground

object ScalaPlayground5 extends App{

  def hello(inputs: String*): Unit =
  {
    for(
      input <- inputs
    ) println(input);
  }

  hello(Seq("hello", "world") : _*)

  Seq("apple", "banana", "grape") match {
    case head +: _ => println(head)
    case _ => println("no match");
  }

  val variable1:Option[String] = Some("IOS");

  val mapped = variable1.map(_.toLowerCase()).flatMap{
    case "ios" => Some("IDFA")
    case "android" => Some("AAID")
    case _ => None
  }

  if(mapped.isDefined)
    {
      println(mapped.get);
    }

}
