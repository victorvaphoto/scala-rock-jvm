package playground

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object ScalaPlayground3 extends App {

  var myQueue = new mutable.Queue[String]
  myQueue.enqueue("hello")
  myQueue.enqueue("wolrd")
  myQueue.enqueue("lala")

  var fruits = new ListBuffer[String]()

  for (i <- 0 until myQueue.size) {
    fruits += myQueue.dequeue();
  }

  println(fruits);
  println(myQueue);
  println(myQueue.size);


}
