package playground;

import java.util.HashMap;
import java.util.Map;

public class JavaPlayground {

    public static void main(String[] args) {
        Car car = new Tank();

        car.drive();
        System.out.println(car.name);

        Tank tank2 = new Tank();
        System.out.println(tank2.name);


        Map<Integer, ? extends Car> myMap = new HashMap<Integer, Car>();
    }

    private void add(Map<Integer, Car> map, Tank tank){
        map.put(1, (Car)tank);
        map.put(1, tank);
    }
}


class Generic<A>{


}

class Car{

    public Car(){

    }
    public Car(String name) {
        this.name = name;
    }

    protected String name = "Car";
    protected void drive(){
        System.out.println("zoom zoom");
    }

}

class Tank extends Car{
    @Override
    protected void drive() {
        System.out.println("smash smash");
    }

    String name = "tank";
}