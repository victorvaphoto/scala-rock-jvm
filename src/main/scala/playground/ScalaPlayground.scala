package playground

import lectures.playground.MyCaseClass

object ScalaPlayground {

  def main(args: Array[String]): Unit = {

    println("hello, scala")

    for (x <- 1 to 5) println(x)

    val car: Car = new Car();
    car.`drive`;
    car.drive();

    class MyClass {

      def poop = println("poop");
    }

    //    val myClass:MyClass = new MyClass();
    //    myClass.`poop`  //WTF is this needed in scala
    //
    //    val option1 = Some("yes")
    //    val option2 = Some("yes2")
    //    val option3 = Some("")
    //    val option4 = None
    //
    //    Seq(option1, option2, option3, option4).flatten.foreach(println)
    //
    //    //without match is an anamously function
    //    List(41, "cat") map { case i: Int ⇒ i + 1 }
    //
    //
    //    //if it matches partially, it's partial function
    //    List(41, "cat") collect { case i: Int ⇒ i + 1 }
    //
    //    val isVowel = Set('a', 'e', 'i', 'o', 'u')
    //    isVowel('a') //apply method of set ...

    //    val list1 = 1 :: 2 :: Nil
    //    var list2 = 3 :: 4 :: Nil
    //    list2.foreach(println)
    //    list2 = list2 ::: list1
    //    list2.foreach(println)

    //the following are equivalent
    //val mergedStdLocRecords2 = listwhatever.flatMap(_._2)
    //val mergedStdLocRecords2 = listwhatever.flatMap(foo => foo._2)


    //Some stuff on lists
    //    val list3 = 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: Nil
    //    println("************************");
    //    list3.init.foreach(println) //everything except the last
    //    println("************************");
    //    println(list3.head)
    //    println("************************");
    //    list3.tail.foreach(println)
    //
    //    val result = list3.tail.foldLeft(list3.head){ (accm, item) =>
    //      {
    //        accm + item
    //      }
    //    }
    //
    //    println("got result " + result);
    //

    //Fold Left example operating on pair, merging right to left optinally
    case class Data(closeEnough: Boolean, var count: Int) {}

    val dataList = Seq(Data(false, 1), Data(true, 1), Data(false, 1), Data(true, 1));
    dataList.foreach(println)
    println("**************************");
    //just operates on things making up the list
    val updatedData = dataList.tail.foldLeft(dataList.head) { (ref, next) => {
      if (next.closeEnough) {
        ref.count = next.count + 1
        ref
      } else {
        next
      }
    }
    }

    //dataList.foreach(println)
    dataList.filter(!_.closeEnough).foreach(println)


    val func = (x: Int) => x % 2 == 0
    val func1:Int => Boolean = x => x % 2 == 0

    //List.range(1, 10).filter(_ % 2 == 0)

    val fruit = Array("Banana", "Apple", "PineApple")
    for((count, item) <- fruit.zipWithIndex)
      {
        println(s"$count is $item");
      }
  }


}
