package playground

import akka.actor.ActorSystem

object AkkaPlay extends App{


  val actorSystem = ActorSystem("HelloAkka")
  println(actorSystem.name)

}
