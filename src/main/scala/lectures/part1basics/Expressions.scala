package lectures.part1basics

object Expressions extends App {

  val x = 1 + 2 //EXPRESSION
  println(x)


  println(2 + 3 * 4)
  println(1 == x)

  println(!(1 == x))

  var aVariable = 2
  aVariable += 3

  println(aVariable)

  // Instructions (Do) vs Expressions

  // IF expression
  val aCondition = true
  val aConditionedValue = if (aCondition) 5 else 3 //IF Expression
  println(if (aCondition) 5 else 3)

  var i = 0
  while (i < 10) {
    print(i)
    i += 1
  }

  //NEVER WRITE THIS AGAIN !!

  //WHILE LOOP IS IMPERATIVE PROGRAMMING

  //EVERYTHING IN SCALA IS AN EXPRESSION !!!!

  val aWeirdValue: Unit = (aVariable = 3)  //UNIT === void

  //aWeirdValue is an UNIT

  //side effect: println(), whiles, reassiging
  //they are expressions returning UNIT


  val aCodeBlock = {
    val y = 2
    val z = y + 1

    if(z > 2) "hello" else "goodbye"

  }


  //a code block is an expression
}
