package lectures.part1basics

object StringOps extends App{

  val str:String = "Hello, I am learning Scala"

  println(str.charAt(2))
  println(str.substring(7, 11))
  println(str.split("").toList)

  println(str.startsWith("Hello"))

  //SCALA things
  //s-interpolator
  val name = "David"

  val age = 12

  val greeting = s"Hello, my name is $name and I am $age years old"
  println(greeting);

  val name2 = "Tom"
  println(s"Hello my name is $name2")

}

