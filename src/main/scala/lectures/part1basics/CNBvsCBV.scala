package lectures.part1basics

object CBNvsCBV extends App{

  def callByValue(x: Long): Unit = {
    println("by value: " + x)
    println("by value: " + x)
  }


  def callByName(x: => Long): Unit = {
    println("by value: " + x)
    println("by value: " + x)
  }

  callByValue(System.nanoTime());

  //expression passed literally
  callByName(System.nanoTime());
}
