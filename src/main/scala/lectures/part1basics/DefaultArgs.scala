package lectures.part1basics

object DefaultArgs extends App{

  //start with sample factorial

  def tailFact(n: Int, acc: Int = 1): Int = {
    if(n <= 1) return acc;
    else return tailFact(n -1, n*acc);
  }

  val fact10 = tailFact(3, 1);
  //print(fact10);

  print(tailFact(4))

  def savePicture(format: String = "jpg", width: Int = 1920, height: Int = 1080): Unit = println("saving pictures")
  //savePicture(800) //won't work, default param cannot be first

  //alternatively, name the arguments
  savePicture(width = 2000)
}
