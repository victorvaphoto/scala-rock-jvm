package lectures.part1basics

object Functions extends App{

    def aFunction(a: String, b: Int): String = {
      a + " " + b
    }

    println(aFunction("hello", 3))


    def aFunction2(name: String, b: Int): Unit = {

      if (b == 1)
        {
          smallFunction(name)
        }
      else
      {
        aFunction2("la" + name, b - 1)
      }
      def smallFunction(name: String) = {
        println("my name is " + name);
      }
    }

  aFunction2("victor", 3)

    //WHEN NEED LOOPS USE RECURSION
    def aReaptedFunction(aString: String, n: Int): String = {
      if(n == 1) aString
      else aString + aReaptedFunction(aString, n - 1)
    }


    println(aReaptedFunction("hello", 3))

    //A function can return unit
    def aFunctionWithSideEffects(aString: String): Unit = {
      println(aString)
    }

    //a code block can contain auxiliary function
    def aBigFunction(n: Int): Int = {
      def aSmallFunction(a: Int, b: Int) = a + b

      aSmallFunction(n, n -1)
    }

  //a greeting function for kids (name, age => say my name is
   def greet(name: String, age: Int): Unit = {
     println("Hi, muy name is %s and I am %s years old".format(name, age));
   }

  greet("Vicitr", 12)

  //exercise, fibonnaci

  def fibonnaci(limit: Int): Int = {
    if(limit == 1) return 1
    if(limit == 2) return 1
    else return fibonnaci(limit -1) + fibonnaci(limit -2);
  }

  println(fibonnaci(5));

  def factorial(limit: Int): Int = {
    if(limit == 1) return 1
    else return limit * factorial(limit -1);
  }

  println(factorial(5));


  def test(): String = {
    val test:String = "victor this is last line"
    test
  }

  //the following are the same
  println(test());
  println(test);

  //if test is defined without(), just test, need to call just println(test)

}
