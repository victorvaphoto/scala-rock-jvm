package lectures.part1basics

object Recursion extends App{

  //each call of recursion uses a stack frame
  def factorial(n: Int): BigInt = {
    if (n <= 0) 1
    else n * factorial(n - 1)
  }

  println(factorial(5000));
}
