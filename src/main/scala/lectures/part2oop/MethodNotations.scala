package lectures.part2oop

object MethodNotations extends App {


  class Person(val name: String, favoriteMovie: String, val age: Int = 0) {
    def likes(movie: String): Boolean = movie == favoriteMovie

    def +(person: Person): String = s"${this.name} is hanging out with ${person.name}" //valid method name
    def +(nickname: String): Person = new Person(s"${this.name} ${nickname}", this.favoriteMovie)
    def unary_! : String = s"$name what the heck"

    def unary_+ : Person = new Person(this.name, this.favoriteMovie, this.age + 1)

    def apply(): String = s"Hi name is $name and I like $favoriteMovie"

    def learns(thing: String) = s"$name is learning $thing"

    def learnsScala = this learns "Scala"

  }

  val mary = new Person("Mary", "Inception")

  val Tom = new Person("Tom", "Fight club")

  println(mary.likes("Inception "))
  println(mary likes "Inception") //infix notation syntactic sugar, works with single parameter, can skip the dot
  //object method parameter

  println(mary + Tom)

  //prefix notation
  //unary_prefix only works with - + ~ !  and needs to have space like unary_+ :
  println(mary.unary_!) //equivalent
  println(!mary)


  //postfix notation
  //rarely used

  //apply
  println(mary.apply()) //apply is special in scala
  println(mary())  //equivalent, if instance is called as a function

  //exercise overload method named +, infix notation, use apply
  println((mary + "rockstar")())


  /**
    * def apply[T](body: =>T)(implicit @deprecatedName('execctx) executor: ExecutionContext): Future[T] =
    *     unit.map(_ => body)
    */


  class Car (val color:String) {

    def apply(param: String) = {
      println(s" this car has color $color and param $param");
    }

    def b(another:Car) = {
      print(s"this car which is color $color is together with " + another.color);
    }
  }

  var toyota:Car = new Car("red");
  toyota("WTF");

  var nissan:Car = new Car("black");


  //syntactic sugar for single method on a class
  toyota b nissan //why would anyone do this, but works in Scala

}
