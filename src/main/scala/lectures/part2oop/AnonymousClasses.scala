package lectures.part2oop

object AnonymousClasses extends App {

  abstract class Animal{
    def eat: Unit
  }

  //instantiated an abstract class ?
  //no it's an anonymous class !!
  val funnyAnimal: Animal = new Animal{

    override def eat: Unit = println("hahahah")
  }

  //proof that this is anonymous class
  println(funnyAnimal.getClass)

  class Person(name: String){
    def sayHi: Unit = println(s"Hi my name is $name, how can I help")
  }

  //we can instantiate type and override field or method on the spo
  new Person("Jim"){
    override def sayHi: Unit = println(s"this is override");
  }


}
