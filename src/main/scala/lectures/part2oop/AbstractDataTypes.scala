package lectures.part2oop

object AbstractDataTypes extends App {


  //abstract fields

  //abstract class can not be instantiated
  abstract class Animal{
    val creatureType: String
    def eat: Unit

  }

  class Dog extends Animal{
    override val creatureType: String = "canine"
    override def eat:Unit = println("crunch crunch")
  }


  //traits are like abstract, but they can be inherited
  trait Carnivore{
    def eat(animal: Animal): Unit
  }

  class Crocodile extends Animal with Carnivore{
    override val creatureType: String = "croc"
    override def eat:Unit = println("munch munch")
    override def eat(animal: Animal): Unit = println(s"I am  a croc and I am eating ${animal.creatureType}")
  }

  val dog = new Dog;
  val croc = new Crocodile;
  croc.eat(dog)

  //trait v.s. abstract classes
  //both can have abstract and non-abstract members
  //trait can not have constructor parameters
  //can mix in multiple traits
  //traits are behaviors, abstract class = "type of thing"

  //scala type hierarchy
  //scala.Any  --> scala.AnyRef (like object) --> scala.Null --> scala.Nothing
  //scala.Any --> scala.AnyVal  (Int, Unit, Boolean, Float) --> scala.Nothing

  //Some more examples

  abstract class Movable(var where:String){

    def move = println(s"I am moving in $where");
  }

  object Car{
    def apply(color:String):Car = return new Car(color);
  }

  trait Honk{
    def honk;
  }

  class Car(var color:String) extends Movable("land") with Honk {
    override def move: Unit = println(s"my car which is $color is moving on $where");

    override def honk: Unit = println("honk honk");
  }

  var car:Car = Car("red");
  car.move;
  car.honk;
}

