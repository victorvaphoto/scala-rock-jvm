package lectures.part2oop

object OOBasics extends App {

  val person = new Person("John", 26)
  println(person.age)
  person.greet("Daniel")

  person.noArg[Writer]; //can call without bracket
  val author = new Writer("Charles", "Dickens", 1812)
  val novel = new Novel("Great Expectations", 1861, author)
  println(novel.authorAge)
  println(novel.isWrittenBy(author))
}


//constructor
class Person(name: String, val age: Int){
  // body
  var x = 2 //this is a field

  println(1 + 3)

  def inc = x = x + 1
  //method, use "this" to use class parameter or class field
  def greet(name: String): Unit = println(s"${this.name} Hi $name")

  //overloading
  def greet(): Unit = println(s"Hi, I am $name")

  //multiple constructors
  def this(name: String) = this(name, 0)

  def this() = this("john")


  def noArg[A]() = {};

}


class Writer(firstName:String, surName:String, val year:Int){

  def fullname(): String = this.firstName + this.surName
}

class Novel(name:String, year:Int, author:Writer){
  def authorAge = year - author.year
  def isWrittenBy(author:Writer) = author == this.author
  def copy(newYear: Int) = new Novel(this.name, newYear, author)

}

class Counter(val count: Int){
  def inc = new Counter(count + 1)
  def dec = new Counter(count - 1) //return a new object as supposed to change current instance

  def inc(n: Int) = new Counter(count + n)
  def dec(n: Int) = new Counter(count - n)

}



//class parameters are not fields !!
//convert parameter, add val or var to convert to
//field