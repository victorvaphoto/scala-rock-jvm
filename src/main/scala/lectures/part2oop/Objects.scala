package lectures.part2oop

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global



object Objects {


  //SCALA class does not have class level functionality
  //use object for that
  object Person { //do not have parameters
    //static level functionality
    val N_EYES = 2

    def canFly: Boolean = false

    //apply method used as factory method
    def apply(mother: Person, father: Person): Person = new Person("bobby", false)

    def apply(name:String, murdered: Boolean) = {
      if (murdered) new Person(name, true) else new Person(name, false);
    }
  }

  class Person(val name: String = "", val dead:Boolean = false) {

    //instance level functionality
  }
  //companion

  def main(args: Array[String]): Unit = {
    println(Person.N_EYES)
    println(Person.canFly)
    //Scala object = SINGLETON INSTANCE

    val person1 = Person
    val person2 = Person
    println(person1 == person2)

    val mary = new Person("mary")
    val john = new Person("john")

    val bobbie = Person(mary, john) //calls the factory method apply

    val dead = Person("John", true);
  }




}
