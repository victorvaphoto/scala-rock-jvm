package lectures.part2oop

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Inheritance extends App {

  //single class inheritance
  class Animal {
    val creatureType = "wild"

    def eat = println("nomnom")
  }

  class Cat extends Animal {
    def crunch = {
      eat
      println("crunch crunch")
    }
  }

  val cat = new Cat

  //for no param methods, omit the ()
  //cat.crunch()
  cat.crunch

  //constructors
  class Person(name: String, age: Int)

  class Adult(name: String, age: Int, idCard: String) extends Person(name, age)


  //overriding
//  class Dog extends Animal {
//    override val creatureType: String = "domestic"
//    override def eat = println("crunch crunch")
//  }

  //constructor based variable override
    class Dog( override val creatureType: String) extends Animal {
      override def eat = println("crunch crunch as a dog")
      def newMethod:Unit = println("hello world")
    }

  val dog = new Dog("domestic")
  dog.eat;
  println(dog.creatureType);

  //type substitution
  val unknownAnimal: Animal = new Dog("K9")
    //method call go to most overriden version

  unknownAnimal.eat;
  println(unknownAnimal.creatureType);


  //another example *******************************

  //companion object
  object Drink{
    def apply() = new Drink();
  }

  class Drink(){
    protected val condiments:ArrayBuffer[String] = ArrayBuffer();

    def taste = println(s"plain and boring")

  }

  //companion object
  object BubbleTea{
    def apply() = new BubbleTea();
  }

  class BubbleTea extends Drink(){

    condiments += "bubble " += "tea " + "sugar "

    override def taste: Unit = {
      condiments.foreach(x => println(x))
    }
  }

  val drink:BubbleTea = BubbleTea()
  drink.taste


}
