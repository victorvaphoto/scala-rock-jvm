package lectures.part2oop

object CaseClasses extends App{

  /*
    equals, hashCode, toString
    case class is useful for templating
   */

  case class Person(name:String, age:Int)

  //use of case class encourages immutable types, use of var is discouraged

  //1. class parameters are promoted to val fields
  val jim = new Person("jim", 34)
  println(jim.name); //it's valid


  //2. sensible to string
  println(jim)

  //3. equals and hash code implemented OOTB

  val jim2 = new Person("jim", 34)
  println(jim == jim2)

  //4. CC have handy copy methods
  val jim3 = jim.copy(age = 45)
  println(jim3)

  //5. CC have companion objects
  val thePerson = Person //compiler automatically create this companion object (singleton object)
  val mary = Person("Mary", 23)

  //6. CC are serializable
  //Akka

  //7. CCS have extractor patterns = CCs can be used in Pattern Matching

  //case object do not have companion object
  case object UnitedKingdom{
    def name: String = "The UK of GB and NI"
  }

  

}
