package lectures.part2oop

import java.util.{Date => UTIL_DATE}
import java.sql.{Date => SQL_DATE}

object PackagingAndImports extends App{



  //import alias
  val date = new UTIL_DATE()

  //package object ?? rarely used in practice
  sayHello //this is in package object
  println(SPEED_OF_LIGHT)


}
