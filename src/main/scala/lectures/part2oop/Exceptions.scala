package lectures.part2oop

object Exceptions extends App {

  val x: String = null
  //println(x.length)

  //this will crash with NPE
  //1. throwing and catching exceptions


  // val aWeirdValue = throw new NullPointerException

  //throwable classes extend the Throwable class
  //Exception and error are the major throwable subtypes

  //2. how to catch exception

  def getInt(withExceptions: Boolean): Int =
    if (withExceptions) throw new RuntimeException("No int for you")
    else 42



  val potentialFail = try {
    getInt(true)
  } catch {

    case e: RuntimeException => println("caught a run time exception ")

  } finally {
    println("finally")
    //optional
    //does not influence the return type of this expression
    //use finally only for side effects
  }

  //how to define your own exception
  class MyException extends Exception
  val exception = new MyException

  throw exception

  /*
    1. crash your program with an out of memory error
    2. crash with SOError
    3. PocketCalculator
       add(x, y)
       subtract(x, y)
       multiply(x, y)
       divide(x, y)


       throw OverflowException

   */
  //OOM

  //val array = Array.ofDim(Int.MaxValue)


  //SO
  //def infinite: Int = 1 + infinite
  class OverflowException extends  RuntimeException
  class UnderflowException extends  RuntimeException

  object PocketCalculator{
    def add(x: Int, y: Int): Int = {
      val result = x + y
      if(x > 0 && y > 0 && result < 0) throw new OverflowException //if x + y becomes negative, it means it overflow int.max
      if(x < 0 && y < 0 && result > 0) throw new UnderflowException
      result
    }

  }

}
