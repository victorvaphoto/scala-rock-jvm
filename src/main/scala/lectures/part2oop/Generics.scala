package lectures.part2oop

object Generics extends App {

  class MyList[+A] {
    //use type A in class definition

    //wont work because of covariant problem
    //def add(element: A): MyList[A] = ???
    def add[B >: A](element: B): MyList[B] = ???
    //A cat
    //B animal
    //if to a list of cat I add a dog, will turn the list into animal

  }

  val listOfIntegers = new MyList[Int]
  val listOfStrings = new MyList[String]

   //object can not be type parameterized
   object Mylist{

     //example of method signature with type parametrized
     def emtpy[A]: MyList[A] = ???

   }

  val emptyListOfInteger = Mylist.emtpy[Int];

  //variance problem

  class Animal
  class Cat extends Animal
  class Dog extends Animal

  //1. yes List[Cat] extends List[Animal]

  //class is covariant in its type parameter A
  class CovariantList[+A]
  val animalLIst: CovariantList[Animal] = new CovariantList[Cat]
  //animalList.add(new Dog) ??? HARD QUESTION --> we return list of animals


  //2. No = Invariance
  class InvariantList[A]
  val invariantAnimalList: InvariantList[Animal] = new InvariantList[Animal]


  //3 hell no, Contravariant
  class Trainer[-A]
  val trainer: Trainer[Cat] = new Trainer[Animal]

  def setPerson(gc: Trainer[Cat]): Unit = ???
  //okay setPerson(new Trainer[Cat])
  //okay setPerson(new Trainer[Animal])

  //bounded types
  class Cage[A <: Animal](animal: A){}
  val cage = new Cage(new Dog)


  //expand MyList to be generic

}
