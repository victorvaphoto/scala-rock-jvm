package lectures.playground

object DarkSyntax extends App {


  //method with single param
  def foo(arg: String) = {
    println(arg);
  }

  foo{"mary"}

  //single abstract method

  trait Noise {
    def makeSound(sound:String);
  }


  val boo: Noise = (x: String) => {println(s"boo boo $x")}

  boo.makeSound("hello")

  //in the same token, this is why thread can take a lambda in place of a something:Runnable =  new Runnable ....
  val thread1 = new Thread(() => println("hello world"))
  thread1.start();

}
