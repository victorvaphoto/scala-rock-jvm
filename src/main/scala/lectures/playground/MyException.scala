package lectures.playground


import scala.util.{Success, Try}

object MyException extends App{



  def betterSafeMethod(): Try[String] = Success("Hello world");
  def betterUnSafeMethod(): Try[String] = util.Failure(new RuntimeException())

  val try1:Try[String] = betterUnSafeMethod().map(str => {println(str + ""); str + "";});
  //try1 will be Failure

  println(try1.isSuccess);

  val try2 = betterSafeMethod().map(str => {println(str + ""); str + "";});

  //try2 will be Success("...")

  println(try2.isSuccess);


}
