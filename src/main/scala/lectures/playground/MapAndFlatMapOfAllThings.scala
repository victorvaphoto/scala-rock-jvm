package lectures.playground

object MapAndFlatMapOfAllThings extends App{

  //Option
  val integer1 = Some(2)

  val integer2 = Some(3)

  val integer3:Option[Integer] = None


  //map operate on value, map to another value
  val result1:Option[Int] = integer1.map(x => x* 2)
  println(result1.getOrElse(0)) //4

  //flatmap to anothger Option
  val result2:Option[Int] = integer1.flatMap(x => Some(x + 10))
  println(result2.getOrElse(0)) //12

  //for each will act upon if Option is defined
  result2.foreach(println) //12

  val result3 = integer3.map(x => x * 2)
  println(result3.isDefined) //false
  println(result3.getOrElse(0)) //0

  //for each will act upon if Option is defined
  result3.foreach(println) //nothing


  val myFunc : (Int, Int) => Option[Int] = (x, y) => Some(x + y)

  //can use a function that takes operate on values and return Option
  val chainedFM = integer1.flatMap(x => integer2.flatMap(y => myFunc(x, y)))
  println(chainedFM.isDefined) //true
  println(chainedFM.getOrElse(0)) //5

  val chainedFM2 = integer1.flatMap(x => integer3.flatMap(y => myFunc(x, y)))
  println(chainedFM2.isDefined) //false
  println(chainedFM2.getOrElse(0)) //0


  val myFuncMap : (Int, Int) => Int = (x, y) => x + y

  val chaineMap = integer1.map(x => integer2.map(y => x + y)) //Option[Option[Int]] not quite what I expected

}
