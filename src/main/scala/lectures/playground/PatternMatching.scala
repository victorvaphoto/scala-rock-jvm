package lectures.playground

object PatternMatching extends App{


  //1. pattern matching is good for decomposing
  case class Person(name:String, age: Int)
  val bob = Person("Bob", 20)

  //can pattern match against case class and decompose/extract values
  //note extractor only works with case class, regular class needs some extra work
  // can use guards
  //1. match in order
  //2. if no match, match error
  //3. type of pattern matching expression is unified type
  val greeting = bob match {
    case Person(n, a) if a < 21 => s"hi $n and I am $a years "
    case Person(n, a) if a >= 21 => s"hi $n and I am $a years old dude "
    case _ => "I don't know "
  }


  //compiler will unify all returned, in this case it's Any
  val x = 3;

  val description = x match {
    case 1 => "The One"
    case 2 => "double or nothing"
    case 3 => "third time is the charm"
    case _ => 25
  }

  //PM on sealed hierarchy, why is this good, because all known
  //sub class needs to be declared here, some compiler can throw warning

  sealed class Animal
  case class Dog(breed:String) extends Animal
  case class Parrot(greeting:String) extends Animal

  val animal:Animal = Dog("Terra Nova")
  animal match {
    case Dog(someBreed) => println(s"Mathced a dog of $someBreed")
    case Parrot(greeting) => {
         def addQuote(greeting:String) = {"\"" + greeting + "\""} //can have method here  !!

         addQuote(greeting)
    }
  }

  //7 name binding
  //Seq(1, Seq(1, 2))
  val aList = Seq(1, Seq(1, 2))

  val namedBinding = aList match{
    case Seq(head, subMatch @ Seq(subhead, subtail)) => subMatch
  }

  //namedBinding is now the Seq(1,2)
  //this is voodoo man
  namedBinding.foreach(println)
}
