package lectures.playground

object MyCaseClass extends App {

  class Person(val name:String, val age:Int)

  //need to have the override since we are overriding the fields
  class Woman(override val name:String, override val age:Int) extends Person(name, age)




}
