package lectures.playground

object HOF extends App{

  //HOF is basically taking a function as a param, or return a function

  //a function, that takes x, returns another functdion that takes y
  val superAdder: Int => (Int => Int) = (x) => ((y) => (x + y));

  val superAdd3 = superAdder(3);

  println(superAdd3(5));

  //defined as HOF

  //two parameters, one is a int, second param is a function that takes two int, that return a Int
  def superAdderCurry(x: Int)(y: Int)(f: (Int, Int) => Int): Int = {
      f(x, y)
  }



}
