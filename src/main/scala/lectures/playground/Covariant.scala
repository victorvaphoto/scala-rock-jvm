package lectures.playground

object Covariant extends App{

  abstract class Animal{
    def sound
  }

  class Dog extends Animal{
    override def sound: Unit = print("woof woof")
  }

  class Cat extends Animal{
    override def sound = print("meow meow");
  }

  //won't work
//  class Birth[A]{
//    def birth: Birth[Animal] = return new Birth[Cat]
//  }

    //make class covariant in its type parameter A in this class, allowing a super class to represent a sub class
    class Birth[+A]{
      def birth: Birth[Animal] = return new Birth[Cat]
    }

    //covariant in A param, invariant in B param
    class Birth2[+A, B]{
      def birth:Birth2[Animal, Animal] = return new Birth2[Cat, Animal]
    }

}
