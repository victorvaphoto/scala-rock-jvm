package lectures.playground
import akka.actor.FSM.Failure
import lectures.part4PatternMatching.FuturePromises.aFuture

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try};

object MyFuture extends App {


  def calculateSomething(): String = {
    Thread.sleep(2000)
    "Victor"
  }

  val aFuture = Future({
    calculateSomething()
  })
  

  aFuture.onComplete{
    case scala.util.Success(value) => println(s"meaning of life is $value")
    case scala.util.Failure(exception) => println("failed")
  }

  Thread.sleep(3000)


}
