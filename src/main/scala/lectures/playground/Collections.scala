package lectures.playground

object Collections extends App{




  def doSomething(param: Seq[Int]) = {

    param.foreach(println)

  }

  val list = 1 :: 2 :: 3 :: Nil

  doSomething(list)
}
