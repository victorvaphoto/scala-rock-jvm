package lectures.playground

import scala.util.Try

object MyOptions extends App{

  def doSomething = null;

  val nullWrapper:Option[String] = Option(doSomething)
  println(nullWrapper.getOrElse("got default "));

  val myFirstOption:Option[(String, Int)] = Some(("victor", 29))

  //map and flatmap will return NONE if NONE is passed in
  myFirstOption.map(x => Some((x._1, x._2 * 2))).filter(_.isDefined);

  //Option[Some]
  var option1 = myFirstOption.map(x => Some((x._1, x._2 * 2)));

  //Option[(String, Int)]
  var option2 = myFirstOption.map(x => (x._1, x._2 * 2));


  //filter operate on the thing that Option is wrapped around !!
  //myFirstOption.map(x => (x._1, x._2 * 2)).filter()

  //good try catch handling technique in Scala

//  .map(line => {
//    Try {
//      val lat = getValueFromLine(line, delimiter, latLocation).toDouble
//      val lng = getValueFromLine(line, delimiter, lngLocation).toDouble
//      Some(line, lat, lng)
//    }.getOrElse(None)
//  }).filter(_.isDefined)
//    .map(_.get)
//    .mapAsync(10) { case (line: String, lat: Double, lng: Double) => {
//      val result = reverse(lat, lng, requestorId, apiKeyId)


  val myOptionFunc: String => Option[String] = x => None;

  //flat map operates on value of Option and return another Option
  //map

  //prints out None, because first flatmap says if not None, then do something
  println(myOptionFunc("hello").flatMap(opt => Some("Hello")).map(str => str.toUpperCase));


  val optionSeq:Seq[Int] = Seq(Some(3), Some(2), None).flatten;
  val result = optionSeq.foldLeft(0){
    (a, b) => a + b
  }

  println("fold left result " + result);


}
