package lectures.playground

object MyFunction extends App {

  //define a regular function
  val function1 = new Function[String, String] {

    override def apply(v1: String): String = ???
  }

  //note in terms of thinking in class, we are instantiating something like
  trait MyFunction[A, B] {
    def apply(element: A): B
  }

  //so new Function2 followed by type definition
  //then override the apply
  val function2 = new Function2[String, String, Function1[String, String]] {
    override def apply(v1: String, v2: String): Function1[String, String] = {
      return new Function1[String, String] {
        override def apply(v3: String): String = {
          return (v1 + v2 + v3);
        }
      }
    }
  }

  //also since String => String is syntactic sugar/type definition for Function1[String, String], can shorten above like

  val function3 = new Function2[String, String, String => String] {
    override def apply(v1: String, v2: String): String => String = {
      return new Function1[String, String] {
        override def apply(v3: String): String = {
          return (v1 + v2 + v3);
        }
      }
    }
  }

  println(function3("mary", "tom")("bob"));

  //  //or as anonomous function ... which use the syntactic sugar for function 2
    val function4: (String, String) => (String => String) = (v1, v2) => {
      val function1: String => String = v3 => {v1 + v2 + v3};
      function1 //can we avoid explicit return ?
    };

  println(function4(" Adela ", "Peety ")(" Gilbert "));

//  val myFun: String => String = v1 => {v1 + "lala";}

  //things that are equivalent, syntactic sugar/type definition
  /*

    string => string                            Function1[String, String]
    (string, string) => string                  Function2[String, String, String]
    (string, string) => (string => string)      Function2[String, String, Function1[String, String]]

    so for anonomous function, just

    Var variableName: syntacic sugar = (v1, v2, v3....etc ) => { } where variable v1, v2, v3 will be auto boound
   */

}
