package lectures.playground

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.util.ByteString
import akka.stream._
import akka.stream.scaladsl._
import akka.{ NotUsed, Done }
import akka.actor.ActorSystem
import akka.util.ByteString
import scala.concurrent._
import scala.concurrent.duration._
import java.nio.file.Paths
import scala.concurrent.ExecutionContext.Implicits.global;


object Akka extends App{

  implicit val sys = ActorSystem("MyTest")
  implicit val mat = ActorMaterializer()

  val source: Source[Int, NotUsed] = Source(1 to 100)
  source.runForeach(i => println(i))(mat)


  val factorials = source.scan(BigInt(1))((acc, next) => acc * next)

  val result: Future[IOResult] =
    factorials
      .map(num => ByteString(s"$num\n"))
      .runWith(FileIO.toPath(Paths.get("factorials.txt")))

  result.onComplete{
    case scala.util.Success(IOResult(count, status)) => println("count" + count);
    case scala.util.Failure(exception) => println("failed")
  }



//  Source(0 to 10).map(n ⇒
//    if (n != 5) n.toString
//    else throw new RuntimeException("Boom!")
//  ).recover {
//    case _: RuntimeException ⇒ "stream truncated"
//  }.runForeach(println)



}
