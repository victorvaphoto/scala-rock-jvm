package lectures.part4PatternMatching

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future, Promise}
import scala.util.{Random, Success}
import scala.concurrent.duration._

object FuturePromises extends App {

  def calculateMeaningOfLife: Int = {
    Thread.sleep(2000)
    42
  }

  val aFuture = Future({
    calculateMeaningOfLife
  })

  //  aFuture.onComplete(t => t match{
  //    case scala.util.Success(value) => println(s"meaning of life is $value")
  //    case scala.util.Failure(exception) => println("failed")
  //  })

  /**
    * “A partial function of type PartialFunction[A, B] is a unary function where the domain does not necessarily include all values of type A. The function isDefinedAt allows [you] to test dynamically if a value is in the domain of the function.”
    */

  //can be simplified to partial functions
  aFuture.onComplete {
    case scala.util.Success(value) => println(s"meaning of life is $value")
    case scala.util.Failure(exception) => println("failed")
  }

  Thread.sleep(3000)

  //mini social network
  case class Profile(id: String, name: String) {
    def poke(anotherProfile: Profile) = println(s"${this.name} poking ${anotherProfile.name}")
  }

  object SocialNetwork {
    val names = Map(
      "fb.id.1-zuck" -> "Mark",
      "fb.id.2-bill" -> "Bill",
      "fb.id.0-dummy" -> "Dummy"
    )

    val friends = Map(
      "fb.id.1-zuck" -> "fb.id.2-bill"
    )

    val random = new Random()

    //API
    def fetchProfile(id: String): Future[Profile] = Future {
      Thread.sleep(random.nextInt(300))
      Profile(id, names(id))
    }

    def fetchBestFriend(profile: Profile): Future[Profile] = Future {
      Thread.sleep(random.nextInt(400))
      val bfId = friends(profile.id)
      Profile(bfId, names(bfId))
    }

  }

  //client: mark to poke bill
  //can write this way, but hard to maintain, Bill is embedded
  val mark:Future[Profile] = SocialNetwork.fetchProfile("fb.id.1-zuck")

//  mark.onComplete({
//    case Success(markProfile) => {
//      val bill = SocialNetwork.fetchBestFriend(markProfile)
//      bill.onComplete({
//        case Success(billProfile) => markProfile.poke(billProfile)
//        case scala.util.Failure(e) => e.printStackTrace();
//      })
//    }
//    case scala.util.Failure(ex) => {
//      ex.printStackTrace();
//    }
//  })
//
  //  Thread.sleep(1000)

  //can be rewritten using for-comprehension

  //functional composition of futures
  //map, flatmap, filter

  //one future of one type to future of another type
  val nameOnTheWall = mark.map(profile => profile.name) //if original future fails, this future will fail with same exception

  val marksBestFriend = mark.flatMap(profile => SocialNetwork.fetchBestFriend(profile))

  val zucksBestFriendRestricted = marksBestFriend.filter(profile => profile.name.startsWith("2"))

  //the recommended way is to use for comprehensions and fall backs
  for{
    mark <- SocialNetwork.fetchProfile("fb.id.1-zuck")
    bill <- SocialNetwork.fetchBestFriend(mark)
  } mark.poke(bill)


  Thread.sleep(1000)

  //fall backs
  //return a well defined profile
  val aProfileNoMatterWhat = SocialNetwork.fetchProfile("unkown id").recover{
    case e: Throwable => Profile("dummy", "forever-alone")
  }

  //return a future
  val aFetchedProfileNoMatterWhat = SocialNetwork.fetchProfile("unkown id").recoverWith{
    case e: Throwable => SocialNetwork.fetchProfile("dummy")
  }

  //equivalent to, use either first or fall back, and if fall back fails, exception from first Future is included
  val aFetchedProfileNoMatterWhat2 = SocialNetwork.fetchProfile("unkown id").fallbackTo(SocialNetwork.fetchProfile("dummy"))

  //YET another example
  case class User(name: String)
  case class Transaction(sender: String, receiver: String, amount:Double, status: String)

  object BankingApp{
    val name = "Rock the JVM banking"

    def fetchUser(name:String): Future[User] = Future{
      Thread.sleep(5000)
      User(name)
    }

    def createTransaction(user:User, merchantName: String, amount: Double): Future[Transaction] = Future{
      Thread.sleep(1000)
      Transaction(user.name, merchantName, amount, "SUCCESS")
    }

    def purchase(username: String, item: String, merchantName: String, cost: Double): String = {
      //fetch the user from the DB

      //create a  transaction

      //wait for transaction to finish
      val transactionStatusFuture = for{
        user <- fetchUser(username)
        transaction <- createTransaction(user, merchantName, cost)
      } yield transaction.status //needs the yield keyword in order to return

      //returns result within
      Await.result(transactionStatusFuture, 2.seconds) //implicit conversions -> pimp my library
      //returns same future
      //Await.ready() //returns
    }

    println(BankingApp.purchase("Daniel", "iphone", "rock jvm store", 3000));

    // promises, a controller over a future
    val promise = Promise[Int]()
    val future = promise.future

    //thread 1 consumer
    future.onComplete({
      case Success(r) => println("I have received " + r)
    })

    //thread 2 producer
    val producer = new Thread(() => {
      println("crunching numbers");
      Thread.sleep(1000)
      //fullfilling the promise
      promise.success(42)
      println("done");

    })
  }

}

