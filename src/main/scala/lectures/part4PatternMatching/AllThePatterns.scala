package lectures.part4PatternMatching

object AllThePatterns extends App{

  //what can we use as pattern
  val x:Any = "Scala"
  val constants = x match{
    case 1 => "a number"
    case "Scala" => "The scala"
    case true => "The truth"
    case AllThePatterns => " a singleton object "
  }

  println(constants);

  //2 - match anything
  //2.1 wild card
  val matchAnything = x match {
    case _ =>
  }

  //2.2 variable
  val matchAVariable = x match{
    case something => s"I have found $something"
  }

  //3 tuple, even nested
  val nestedTuple = (1, (2, 3))
  val matchNestedTuple = nestedTuple match {
    case (_ ,(2, v)) => println(s"I have got $v")
  }

  //PM can be nested
  val aList = Seq(1, Seq(1, 2))
  val matchAList = aList match{
    case Seq(head, Seq(subhead, subtail)) => println(s"I have got $subhead and $subtail");
  }

  //5 crazy list patterns
  val aStandardList = List(1, 2, 3, 42)
  val standardListMatching = aStandardList match {
    case List(1, _, _, _) => //available List extractor
    case List(1, _*) => //arbitrary
    case 1 :: List(_) => //infix pattern
    case List(1, 2, 3) :+ 42 => //infix pattern
  }

  //6 type specifier
  val unknown: Any = List(1, 2, 3, 4)
  val unknownMatch = unknown match{
    case type1: List[Int] => println("got list")  //matches on the type
    case _ => println("got this")
  }

  //7 name binding
  //Seq(1, Seq(1, 2))
  val namedBinding = aList match{
    case Seq(head, subMatch @ Seq(subhead, subtail)) => subMatch
  }

  //this is voodoo man
  namedBinding.foreach(println)

  //multi pattern
  val mutiPattern = aList match{
    case Seq(2, _) | Seq(_, Seq(_, _)) => println("we got match !!!");
  }

  //9 if guard
  case class Test(param1:Int, param2: Int)
  Test(10, 20) match {
    case Test(10, x) if x > 20 => println("greater than 20");
    case Test(10, x) if x <= 20 => println("less or equal than 20");
  }

  /*
  * Questions
  * */


}


