package lectures.part4PatternMatching

import scala.util.Random

object PatternMatching extends App{

  val random = new Random
  val x = random.nextInt(10)

  val description = x match {
    case 1 => "The One"
    case 2 => "double or nothing"
    case 3 => "third time is the charm"
    case _ => "something else "

  }


  println(x)
  println(description)

  //1. decompose values
  case class Person(name:String, age: Int)
  val bob = Person("Bob", 20)

  //pattern match against a case class and decompose fields and use them
  //also can use guards
  val greeting = bob match{
    case Person(n, a) if a < 21 => s"Hi name is $n and I am $a years old and I can not drink"
    case Person(n, a) => s"Hi name is $n and I am $a years old"
    case _ => "I don't know who I am "
  }

  //cases are matched in order
  //what if no case match //scala match error


  //Pattern Matching on sealed hierarchy, works well with case classes
  //if use sealed class, we would get compiler warning below, which is a good guard
  sealed class Animal
  case class Dog(breed: String) extends Animal
  case class Parrot(greeting: String) extends Animal

  val animal: Animal = Dog("Terra Nova")
  animal match{
    case Dog(someBreed) => println(s"Matched a dog of $someBreed breed")
  }


  println(greeting)

  //don't do the following
  val isEven = if(x % 2 == 0) true else false
  //just do
  val isEvenBetter = x % 2 == 0


  //exercise, pattern match can call it self

  /*
  *  takes an expression, human readable form
  *
  *  Sum(Number(2), Number(3)) => 2 + 3
  *  Sum(Number(2), Number(3), Number(4)) => 2 + 3 + 4
  *  Prod(Sum(Number(2), Number(1)), Number(3)) = (2 + 1) * 3
  *  Sum(Prod(Number(2), Number(2)), Number(3)) = 2 * 1 + 3
  * */

  trait Expr
  case class Number(n: Int) extends Expr
  case class Sum(e1: Expr, e2: Expr) extends Expr
  case class Prod(e1: Expr, e2:Expr) extends Expr

  //use an anonymous helper function
  def show(e: Expr):String = e match {
    case Number(n) => s"$n"
    case Sum(e1, e2) => show(e1) + " + " + show(e2)
    case Prod(e1, e2) => {
      def mayBeShow(exp: Expr) = exp match{
        case Sum(_, _) => " ( " + show(exp) + " ) "
        case _ => show(exp)
      }

      mayBeShow(e1) + mayBeShow(e2)
    }
  }

  println(show(Prod(Sum(Number(2), Number(1)), Number(3))))
}
