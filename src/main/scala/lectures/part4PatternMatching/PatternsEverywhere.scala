package lectures.part4PatternMatching

object PatternsEverywhere extends App{

  //big idea #1
  try{

  } catch {
    case e: RuntimeException => "run time"
    case npe: NullPointerException => "npe"
    case _ => "something else"

  }

  //catches are actually Matches
  /*
      is the same as
      catch(e) {
        e match {
          case ...

        }
      }
   */

  //big idea #2
  val list = List(1,2, 3, 4)
  val evenOnes = for{
    x <- list if x %2 == 0  //generator with optional guard is also a pattern
  } yield 10 * x

  def gameResults(): Seq[(String, Int)] = ("Daniel", 3500) :: ("Melissa", 13000) :: ("John", 7000) :: Nil

  //for comprehension (pattern matching on generator) + guard, pretty powerful stuffs
  def hallOfFame = for {
    (name, score) <- gameResults() if (score > 5000)
    } yield name

  hallOfFame.foreach(println)

  val tuple = (1,2, 3)
  val (a, b, c) = tuple
  println(b)


  val head :: tail = list

  //big idea #4
  //partial function in map statement
  val mappedList = list.map{
    case v if v % 2 == 0 => v + "is even"
    case 1 => "the one"
    case _ => "some else"
  }

  println(mappedList)




}
