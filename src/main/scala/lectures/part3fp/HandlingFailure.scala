package lectures.part3fp

import scala.util.{Failure, Random, Success, Try}

object HandlingFailure extends App {

  //create success and failure
  //note will rarely have to create these
  val aSuccess = Success(3)
  val aFailure = Failure(new RuntimeException("super failure"))

  println(aSuccess)
  println(aFailure)

  def unsafeMethod(): String = throw new RuntimeException("NO String for you")

  //Try objects via the apply method
  val potentialFailure = Try(unsafeMethod())
  println(potentialFailure)

  //syntax sugar, calls apply of Try
  val anotherPotentialFail = Try {

  }

  //utilities
  println(potentialFailure.isSuccess)

  //orElse
  def backupMethod(): String = "a Valid result"

  val fallbackTry = Try(unsafeMethod()).orElse(Try(backupMethod()))
  println(fallbackTry) //success(a valid result)

  //similar to Option, if code might throw null, use Option, if code might throw exception, use Try

  def betterUnSafeMethod(): Try[String] = Failure(new RuntimeException)

  def betterBackupMethod(): Try[String] = Success("A valid result")

  val betterFallBack = betterUnSafeMethod() orElse betterBackupMethod()

  //map, flatmap, filter
  println(aSuccess.map(_ * 2))
  println(aSuccess.flatMap(x => Success(x * 10)))
  println(aSuccess.filter(_ > 10))

  //example how to make safe API assuming you have the source code
  val host = "localhost"
  val port = "8080"

  def renderHTML(page: String) = println(page)

  class Connection {
    def get(url: String): String = {
      val random = new Random(System.nanoTime())
      if (random.nextBoolean()) "<html>...</html>"
      else throw new RuntimeException("Connection interrupted")
    }

    def getSafe(url:String): Try[String] = Try(get(url))
  }

  object HttpService {
    val random = new Random(System.nanoTime())

    def getConnection(host: String, port: String): Connection =
      if (random.nextBoolean()) new Connection
      else throw new RuntimeException("someone else took the port")

    def getSafeConnection(host:String, port:String): Try[Connection] = Try(getConnection(host, port))
  }

  //if you get html page from connection, print to console
   HttpService.getSafeConnection(host, port).flatMap(conn => conn.getSafe("/home")).foreach(renderHTML)

  //or alternatively
  for{
    connection <- HttpService.getSafeConnection(host, port)
    html <- connection.getSafe("/home")
  } renderHTML(html)

}
