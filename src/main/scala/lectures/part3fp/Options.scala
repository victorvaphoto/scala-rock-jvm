package lectures.part3fp

import scala.util.{Random, Try}

object Options extends App {

  //Option is a special collection
  val myFirstOption:Option[Int] = Some(4)
  val noOtion:Option[Int] = None

  println(myFirstOption) //prints Some(4)


  //how to handle method that can return null ?
  def unsafeMethod(): String = null

  //Some(unsafeMethod())  //should never do this
  val result = Option(unsafeMethod())
  println(result) //will get NONE

  println("got " + Option(null)) //this is equivalent to null

  //use Option in chained method
  def backupMethod(): String = "A"


  //another pattern, if unsafeMethod returns None, will use backup method
  val chainedResult = Option(unsafeMethod()).orElse(Option(backupMethod()))

  //Design unsafe API that returns option, you can than do this
  def betterUnsafeMethod(): Option[String]= None

  def betterBackupMethod: Option[String] = Some("A Valid Result")

  //so both the above already returns Option

  //val betterResult = betterUnsafeMethod() orElse betterBackupMethod()

  //functions on Options

  println(myFirstOption.isEmpty) //gets false

  println(myFirstOption.get) //UNSAFE, do not use this .....

  //map, flatmap and filter on Option

  //note map operation operate on the value of the Option
  myFirstOption.map((x: Int) => x * 2) //returns Some(8)

  myFirstOption.filter(_ > 10) //returns NONE

  //the following shortcut didn't work for some reason
  //myFirstOption.flatMap(Option(_ + 2))

  //flatmap needs to return same type
  myFirstOption.flatMap(x => Option(x + 10))

  //for comprehensions
  //exercise
  val config: Map[String, String] = Map(
    "host" -> "176.45.36.1",
    "port" -> "80"
  )

  class Connection{
    def connect = "Connected" //parameterless function that returns "connectted"
  }

  //Connection returns an Option
  object Connection{
    val random = new Random(System.nanoTime())


    def apply(host:String, port:String):Option[Connection] =
      if(random.nextBoolean()) Some(new Connection)
      else None
  }

  //try to establish a connection, if so print the connect method

  //both host and port returns options
  val host = config.get("host")
  val port = config.get("port")

  /* one alternative
  if(host.isDefined && port.isDefined)
    {
      val conn = Connection(host.get, port.get)
      if(conn.isDefined) conn.get.connect;
    }

    */

  //another alternative, taking advantage of flatmap and map ability to operate on Option
  //this is the same as saying
  /*
  * if h not null ==> port.flatmap
  * if p is not null ==> connection
  * */

  //map needs to return same type, if called on Option, expects to operate on the value of Option and return Option
  //flat map return different type
  val connection = host.flatMap(h => port.flatMap(p => Connection(h, p)))
  val connectionStatus = connection.map(conn => conn.connect)

  connectionStatus.foreach(println)

  //even more concise
  config.get("host").flatMap(host => config.get("port").flatMap(port => Connection(host, port)).map(_.connect)).foreach(println)

  //better readability with for comprehenion
  val resultFor = for {host <- config.get("host")
       port <- config.get("port")
       connection <- Connection(host, port)
  } yield connection.connect



}
