package lectures.part3fp

object WhatsFunction extends App {

  //use functions as first class elements
  //problem: oop
  val double = new MyFunction[Int, Int] {
    override def apply(element: Int): Int = element * 2

  }

  println(double(2))

  // function types = Function1[A, B]  scala support these up to 22 parameters
  val stringToIntConverter = new Function[String, Int] {

    override def apply(string: String): Int = string.toInt
  }

  //function types Function2[A, B, R] === (A, B) => R
  //ALL SCALA FUNCTIONS ARE OBJECTS

  println(stringToIntConverter("3") + 4)

  //make generic
  trait MyFunction[A, B] {

    def apply(element: A): B

  }


  //exercise
  //1. a function takes 2 strings and concatenate them

  val concatenate: ((String, String) => String) = new Function2[String, String, String]{
    override def apply(a: String, b: String): String = a + b
  }

  //3. define a function which takes an int and returns another function which takes an int and returns an int

  val superAdder = new Function1[Int, Function1[Int, Int]]{
    override def apply(a: Int): Function1[Int, Int] = new Function1[Int, Int]{
      override def apply(y: Int): Int = a + y
    }
  }

  val adder3 = superAdder(3) //returns a function
 // println(adder3(4)) //calls apply on the previous function

  //println(superAdder(3)(4)) //curried function (receives a parameter, return a function)


  println(concatenate("hello", "world"));

  //or as anonymousFunction
  val superAdderAnon : Int => (Int => Int) = a => {b => a + b}

  println(superAdderAnon(3)(4))


  //list v.s. vector
  //list, keep reference to tail
  //updating an element in the middle takes long

  //vector, depth of tree is small
  //needs to replace all 32
  //vector is a lot better for have to randomly replace
}
