package lectures.part3fp

object TuplesAndMaps extends App {

  //tuples = finite ordered "lists"

  //Tuple2[Int, String] = (Int, String)
  //(Int, String) in Scala means tuple
  val aTuple = new Tuple2(2, "hello Scala")
  //alternatively

  val aTuple2 = (2, "hello")

  val aTuple3 = "jim" -> "hello"

  //Tuple can be up to 22 types

  //println(aTuple._1)
  //println(aTuple.copy(_2 = "good by java"))

  //println(aTuple.swap)

  //maps
  //associate keys to values
  val aMap: Map[String, Int] = Map()


  //specify map with tuples
  val phonebook = Map(("Jim", 555), ("Daniel", 789)).withDefaultValue(-1)

  //alternatively "Jim" -> 555 syntactic sugar as tuple
  val phonebook2 = Map("Jim" -> 555, "Daniel" -> 789)


  //works
  phonebook2.contains("Jim")
  phonebook2("Jim")


  // add a paring
  val newPairing = "Mary" -> 678
  val newphoneBook = phonebook2 + newPairing

  //functional on maps
  //map, flatmap, filter

  //map works on pairing
  println(phonebook.map(pair => pair._1.toLowerCase -> pair._2))

  //filter keys
  println(phonebook.filterKeys(x => x.startsWith("J")))
  println(phonebook.filterKeys(_.startsWith("J")))


  //mapValues
  println(phonebook.mapValues(x => x * 10))

  //conversions to other collections
  println(phonebook.toList)

  //fancy function to return
  val names = List("Bob", "James", "Angela", "Mary", "Daniel")
  println(names.groupBy(name => name.charAt(0)))

  //Map(J -> List(James), A -> List(Angela), M -> List(Mary), B -> List(Bob), D -> List(Daniel))

  //exercise
  //over simplified network based on maps
  //add a new person to the network
  def add(network: Map[String, Set[String]], person: String): Map[String, Set[String]] = {
    network + (person -> Set())
  }

  def add(network: Map[String, Set[String]], a: String, b: String): Map[String, Set[String]] = {
    val friendsA = network(a)
    val friendsB = network(b)

    network + (a -> (friendsA + b)) + (b -> (friendsB + a))
  }

  //remove a person from network

  def unfriend(network: Map[String, Set[String]], a: String, b: String): Map[String, Set[String]] = {
    val friendsA = network(a)
    val friendsB = network(b)

    network + (a -> (friendsA - b)) + (b -> (friendsB - a))
  }



}
