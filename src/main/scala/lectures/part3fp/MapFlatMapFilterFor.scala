package lectures.part3fp

object MapFlatMapFilterFor extends App{


  val list = List(1, 2, 3)

  //map
  println(list.head)
  println(list.tail)

  //filter
  println(list.map(_  + 1))
  println(list.filter(_ % 2 == 0))


  //flat map
  val toPair = (x: Int) => List(x, x + 1)

  println(list.flatMap(toPair))

  //print all combinations between two lists
  val numbers = List(1, 2, 3, 4)
  val chars = List('a','b','c','d')

  /*
  for(number <- numbers)
    {
      for (char <- chars)
        {
          println(number + "" + char)
        }
    }
 */

  //for each n, turns it into a list (1a, 1b, 1c, 1d)
  val combinations = numbers.flatMap(n => chars.map(c => "" + c + n))
  //println(combinations)

  var colors = List("black", "white")

  val combinations2 = numbers.flatMap(n => chars.flatMap(c => colors.map(e => "" + c + n + e)))

  //println(combinations2)

  //foreach
  list.foreach(println)

  //alternatively, for-comprehension, easier to read
  val forCombinations = for {
    n <- numbers
    c <- chars
    color <- colors
  } yield " " + c + n + "-" + color


  println(forCombinations)


  //alternatively, for-comprehension, easier to read
  val forCombinationsWGuard = for {
    n <- numbers if n % 2 == 0
    c <- chars
    color <- colors
  } yield " " + c + n + "-" + color


  //another syntax overload
  list.map(x => x + 1)
  list.map {
    x =>
      x + 1
  }
}
