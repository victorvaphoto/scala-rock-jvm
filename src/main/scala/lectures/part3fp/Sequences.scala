package lectures.part3fp

object Sequences extends App{


  // Seq, well defined order, indexed
  val aSequence = Seq(1, 3, 2, 4)

  /*
  println(aSequence)
  println(aSequence.reverse)

  println(aSequence(2))

  println(aSequence ++ Seq(5, 6, 7))  //concatenate
  println(aSequence.sorted)

*/

  // Ranges
  val aRange: Seq[Int] = 1 to 10


  //aRange.foreach(println)

  //(1 to 10).foreach(x => println("hello"))

  //List
  //head, tail, isEmpty are fast 0(1)
  //most ops are O(n)

  //sealed has two subtypes
  val aList = List(1, 2, 3, 4)
  val prepended = 42 :: aList
  val prepended2 = 42 +: aList
  val preAndPost = 42 +: aList :+ 89

  //println(prepended)
  //println(prepended2)
  //println(preAndPost)

  val apples5 = List.fill(5)("apple") //fill is a curry function
  //println(apples5)
  //println(aList.mkString("-:-"))

  //Array, equivalent to simple Java arrays, immutable, interoperable with Java T[]
  val numbers = Array(1, 2, 3, 4)
  val treeElements = Array.ofDim[Int](3)

  //println(treeElements)

  //mutation
  numbers(2) = 0
  println(numbers.mkString(" "))


  //though array is a native java array, can map directly to sequence like so  //implicit conversion
  val numbersSeq: Seq[Int] = numbers

  //vectors, default implementation for immutable sequences
  //good performance for large sizes
  //effectively constant read and write
  val vector: Vector[Int] = Vector(1,2,3)
  println(vector)

  //vectors v.s. lists



}
