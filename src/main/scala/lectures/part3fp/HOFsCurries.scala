package lectures.part3fp

object HOFsCurries extends App{


  //val superFunction: (Int, (String, (Int => Boolean)) => Int) => (Int => Int) = ???

  //higher order function

  //map. flatmap, filter

  //function that applies a function n times over a value x

  //nTimes(f, n, x)
  //nTimes(f, 3, x) = f(f(f(x))) = nTimes(f, 2, f(x))

  def nTimes(f: Int => Int, n: Int, x: Int): Int = {
    if(n <= 0 ) return x
    else
      nTimes(f, n -1, f(x))
  }


  val plusOne = (x: Int) => x + 1
  println(nTimes(plusOne, 10, 1))

  //curried function, transform function with multiple arg into just one arg

  //super adder, given x, returns a function
  val superAdder: Int => (Int => Int) = (x: Int) => (y: Int) => x + y

  //now add3 is a function
  val add3 = superAdder(3) //returns a function with some values initialized which by itself can be called


  //println(superAdder(3)(5))

  //ALTERNATIVELY, use function with multiple parameter list
  //functions with multiple parameter lists to act like curried functions
  def curredFormatter(c: String)(x:Double): String = c.format(x)

  //HAVE to specify type !!
  val standardFormat:(Double => String)= curredFormatter("%4.2f")

  println(standardFormat(Math.PI));

  //another example
  def anotherExample(a: String)(b: String)(f: (String, String) => String) = {
    f(a, b)
  }

  def concate = (a: String, b:String) => a + "::" + b

  val another2:(((String, String) => String)=> String) = anotherExample("yo")("foo")
  val foobar = another2(concate);

  println(foobar);

}
