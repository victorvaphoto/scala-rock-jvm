package lectures.part3fp

object AnonymousFunction extends App{

  val doubler = new Function[Int, Int] {
    override def apply(v1: Int): Int = v1 * 2

  }
  //or
  def doublder4(x: Int) : Int = {
    x * 2
  }

  //as anonymous function or lambda
  val doubler2 = (x: Int) => x * 2 //syntactic sugar for the above

  //same as above, specify type
  val double22: (Int => Int) = (x:Int) => x * 2
  //or
  val doubler3: (Int => Int) = x => x * 2


  //multiple parameters in lambda
  val adder: (Int, Int) => Int = (a: Int, b: Int) => a + b

  //no params
  val justDoSomething = () => 3

  //curl braces with lambdas
  val stringToInt = { (str:String) => str.toInt

  }

  //MORE syntactic sugar, remove the paran
  val niceIncrementer: Int => Int = (x: Int) => x + 1

  //equivalent
  val niceIncrementer2: Int => Int = _ + 1

  val niceAdder: (Int, Int) => Int = (a, b) => a + b

  //equivalent, note each underscore needs to be match to type definition
  val niceAdder2: (Int, Int) => Int =  _ + _ //useful for higher order function




}
