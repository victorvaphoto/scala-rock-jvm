package exercises

abstract class MyList[+A] {


  /*
     head = first element of list
     tail = remainder of list
     isEmpty = is this list empty
     add(int) = new list with this element added
     toString = a string representation of the list

  */

  def head: A
  def tail: MyList[A]
  def isEmpty: Boolean
  def add[B >: A](element: B): MyList[B]
  def printElements: String
  override def toString: String = "[ " + printElements + " ]"

  def map[B](transformer: MyTransformer[A, B]): MyList[B]
  def flatMap[B](transformer: MyTransformer[A, MyList[B]]) : MyList[B]
  def filter(predicate: MyPrediate[A]): MyList[A]
}
/*
object Empty extends MyList[Nothing]{

  def head: Nothing = throw new NoSuchElementException
  def tail: MyList[Nothing] = throw new NoSuchElementException
  def isEmpty: Boolean = true
  def add[B >: Nothing](element: B): MyList[B] = new Cons(element, Empty)

  override def printElements: String = ""
  override def map[B](transformer: MyTransformer[Nothing, B]): MyList[B] = Empty
  override def flatMap[B](transformer: MyTransformer[Nothing, MyList[B]]) : MyList[B]  = Empty
  override def filter(predicate: MyPrediate[Nothing]): MyList[Nothing]  = Empty
}


class Cons[+A](h: A, t: MyList[A]) extends MyList[A]{

  def head: A = h
  def tail: MyList[A] = t
  def isEmpty: Boolean = false
  def add[B >: A](element: B): MyList[B] = new Cons(element, this)

  override def printElements: String = head + " " + tail.printElements

  //override def filter(predicate: MyPrediate[Nothing]): MyList[Nothing]  = Empty
}


object ListTest extends App{
  val list = new Cons(1, new Cons(2, new Cons(3, Empty)))
  println(list.head)

  println(list.toString);

  val listOfIntegers: MyList[Int] = Empty
  val listOfStrings: MyList[String] = Empty
}
*/
trait MyPrediate[-T]{
  def test(elem: T):Boolean
}

trait MyTransformer[-A, B]{
  def transform(elem:A): B
}


/*
* 1. generic trait MyPredicate[-T] with method test(t) => Boolean
* 2. generic trait MyTransform[-A, B] with method transform(A) => B
* 3. MyList:
*      map(transformer) => MyList
*      filter(predicate) => MyList
*      flatMap(transformer from A to MyList[B]) => MyList[B}
*
*      [1,2,3].map(n*2) = [2, 4, 6]
*
* */