package advanced.lectures.part3

import java.util.concurrent.Executors

object Intro extends App{

  //JVM threads
  val aThread = new Thread(() => println("running in paralle"))

  aThread.start(); //gives the signal to the JVM to start a JVM thread

  aThread.join() //blocks until thread finishes running

  val threadHello = new Thread(() => (1 to 5).foreach(_ => println("hello")))

  val threadGoodbye = new Thread(() => (1 to 5).foreach(_ => println("goodbye")))

  threadHello.start()
  threadGoodbye.start() //different runs produce different results

  //executors
  val pool = Executors.newFixedThreadPool(10)
  pool.execute(() => println("something in the thread pool"))

  pool.execute(() => {

    Thread.sleep(1000)
    println("done after one second")
  })


  pool.execute(() => {

    Thread.sleep(1000)
    println("almost done")

    Thread.sleep(1000)
    println("done after 2 seconds")
  })

  pool.shutdown()
  pool.execute(() => println("should not appear")) //throws exception
}
