package advanced.lectures.part2afp

object PartialFunctions extends App {

  //this is a whole function, as operates on all integers
  val aFunction = (x: Int) => x + 1; //Function1[Int, Int] === Int => Int
  val aFunction2: Int => Int = x => x + 1;

  //this is clunky
  val aFussyFunction = (x: Int) =>
    if (x == 1) 42
    else if (x == 2) 56
    else if (x == 5) 999
    else throw new FunctionNotApplicableException

  class FunctionNotApplicableException extends RuntimeException

  val aNicerFussyFunction = (x: Int) => x match{
    case 1 => 42
    case 2 => 56
    case 3 => 999
  }

  //{1, 2, 5} => Int //this maps partial integer domain to integer, this is a partial function from int to int

  val aPartialFunction: PartialFunction[Int, Int] = {
    case 1 => 42
    case 2 => 56
    case 3 => 999
  }
  /*
   {
    case 1 => 42
    case 2 => 56
    case 3 => 999
  }

  is called partial function value
  * */
  println(aPartialFunction(2));
  println(aPartialFunction(8989));

  //PF utilities
  println(aPartialFunction.isDefinedAt(67));

}
