package advanced.lectures.part4

object OrganizingImplicits extends App {

  implicit val reverseOrdering: Ordering[Int] =
    Ordering.fromLessThan(_ > _)

  //  implicit val normalOrdering:Ordering[Int] =
  //    Ordering.fromLessThan(_ < _)
  //will not compile

  println(List(1, 4, 5, 3, 2).sorted)

  //by default scala pulls from here
  //scala.Predef

  /*

  implicits:
  - val/var
  - Object
  - accessor methods = defs with no parenthesis

    //this is important, had we defined implicit def reverseOrdering() this
    would not have worked
   */

  //Exercise
  case class Person(name: String, age: Int)

  val persons = List(
    Person("Steve", 30),
    Person("Amy", 22),
    Person("JOhn", 66)
  )

//  object Person {
//    implicit def nameOrdering: Ordering[Person] = Ordering.fromLessThan((a, b) => a.name.compareTo(b.name) < 0)
//  }
//
//  //this will take take precedence
//  implicit def ageOrdering: Ordering[Person] = Ordering.fromLessThan((a, b) => a.age < b.age)
//
//  println(persons.sorted) //will not work without implicit ordering

  /* implicit scope

      -normal scope = Local Scope
      -imported scope //like Futures
      -companions of all types involved
       -List
       -Ordering
       -all the types involved = A or any super type

       had we defined it like this would not have worked

       object SomeRandom{
        implicit def normalOrdering:Ordering[Person] = Ordering.fromLessThan(_.age < _.age)
       }

       but would work

       object Person{
        implicit def normalOrdering:Ordering[Person] = Ordering.fromLessThan(_.age < _.age)

       }
   */

  /*
     best practice,

     if single value, define implicit in the companion object
     if more than one, explicit import

   */

  object AlphabeticNameOrdering {
    implicit def nameOrdering: Ordering[Person] = Ordering.fromLessThan((a, b) => a.name.compareTo(b.name) < 0)
  };

  object AgeOrdering {
    implicit def ageOrdering: Ordering[Person] = Ordering.fromLessThan(_.age < _.age)
  }


  //import AlphabeticNameOrdering._;
  import AgeOrdering._;
  println(persons.sorted)
}
