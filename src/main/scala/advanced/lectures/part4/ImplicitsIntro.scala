package advanced.lectures.part4

object ImplicitsIntro extends App {

  val pair = "Daniel" -> "555"
  val intPair = 1 -> 2

  case class Person(name: String){
    def greet = s"Hi, my name is $name!"
  }

  implicit def fromStringToPerson(str: String): Person = Person(str)

  //this normally will not compile, but compiler look for any implicit to convert "Peter" into something that would
  println("Peter".greet) //compiler does this //println(fromStringToPerson("Peter").greet


  //two implicits confuses the compiler
//  class A {
//    def greet: Int = 2
//  }
//
//  implicit def fromStringToA(str: String) : A = new A

  //implicit parameters, different way to do parameter defaulting
  def increment(x: Int)(implicit amount: Int):Int = x + amount
  implicit val defaultAmount = 10
  //compiler will bind this in search scope

  increment(2)

}
