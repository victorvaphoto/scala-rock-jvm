package advanced.lectures.part1

object AdvancedPattern extends App{

  val numbers = List(1)
  val description = numbers match{
    case head :: Nil => println(s"only element is $head")
      //case ::(head, Nil) //infix equivalent
    case _ =>
  }

  /*
    pattern matching works on the following
    -constants
    -wildcards
    -case classes
    -tuples
    -some special magic
   */
  //but what if none of the above applies, for example a class that you can not make a case class
  class Person(val name: String, val age: Int)
  object PersonPattern{
    def unapply(person:Person): Option[(String, Int)] =
      if(person.age < 20) None
      else Some((person.name, person.age)) //type of unapply is Option of type you want to decompose

    def unapply(age:Int): Option[String] =
      Some(if (age < 21) "minor" else "major")
  }

  val bob= new Person("Bob", 25)
  val greeting = bob match {
    case PersonPattern(n, a) => s"Hi my name is $n and I am $a yo"
  }

  //will not work as Car is not case
//  class Car(val color: String)
//  val tank = new Car("my tank")
//  tank match{
//    case Car(n) => ""
//  }

  bob.age match{
    case PersonPattern(status) => s"Bob status is $status"
  }

  /* exercise

   */
  val n: Int = 8
  val mathProperty = n match{
    case x if x < 10 => "single digit"
    case x if x % 2 == 0 => "even number"
    case _ => "no property"
  }
  //can be simplified with single object with unapply
  object even{
    def unapply(arg: Int): Option[String] =
      if (arg % 2 == 0) return Some("it's even") //would result a pattern that would only match on even
      else return None
  }

  object single{
    def unapply(arg: Int): Option[String] =
      if (arg < 10) return Some("it's single")
      else return None
  }

  val mathPropertyBetter = n match{
    case even(s) => s
    case single(s) => s
    case _ => "lala"
  }

  println(mathPropertyBetter);

  /**
    * even simplified
    * //can be simplified with single object with unapply
    * object even{
    * def unapply(arg: Int): Boolean =
    * return  (arg % 2 == 0)
    * }
    *
    * object single{
    * def unapply(arg: Int): Boolean =
    * return (arg < 10)
    * }
    *
    * val mathPropertyBetter = n match{
    * case even() => "even"
    * case single() => "single"
    * case _ => "lala"
    * }
    */

  //infix patterns type pattern only works when you have two things in pattern
  case class Or[A, B](a:A, b:B)
  val either = Or(2, "Two")
  val humanDescription = either match{
    //case Or(a, b) => s"$a is written as $b"
    case a Or b => ??? //infix pattern with two parameters
  }


  //custom return type for unapply, this is really RARE so who cares
  //just need to implement isEmpty: Boolean, get: something

  abstract class Wrapper[T]{
    def isEmpty: Boolean
    def get: T
  }

  object PersonWrapper{
    def unapply(person:Person): Wrapper[String] = new Wrapper[String]{
      override def isEmpty: Boolean = false

      override def get: String = person.name
    }
  }

  bob match{
    case PersonWrapper(n) => s"This person nameis $n"
    case _ => "An alien"
  }


}
