package advanced.lectures.part1

import scala.util.Try

object DarkSugars extends App {

  //method with single param can be substituted with {} that returns the arg
  def singleArgMethod(arg: Int): String = s"$arg little ducks ..."

  val description = singleArgMethod(2);

  //is the same as
  singleArgMethod {
    //do something
    //do something
    2
  }

  val aTryInstance = Try{
    throw new RuntimeException
  }

  (1 :: 2 :: 3 :: Nil).map{x => x + 1}

  //syntax sugar #2 single abstract method
  trait Action {
    def act(x: Int):Int
  }

  //anonymous class
  val anInstance: Action = new Action{
    override def act(x: Int): Int = x + 1
  }

  //voodoo magic, single abstract method, just write the lambda that implements the single method of abstract class
  val aFunkyInstance: Action = (x: Int) => x + 1

  //example: Runnables
  val aThread = new Thread(new Runnable {
    override def run(): Unit = println("hello")
  })

  val aSweeterThread = new Thread(() => println("hello"))


  //also works on abstract class
  abstract class AnAbstractType{
    def implemented: Int = 23
    def f(a: Int): Unit
  }

  //implement the abstract class by specifying the lambda
  val anAbstractTypeImpl: AnAbstractType = (a: Int) => {a}

  //syntac sugar #3 the :: and #"" methods
  val prependedList = 1:: 2 :: 3 :: List(4, 5)
  println(prependedList);
  //scala last char decides associativity of method
  val prependedList2 = List(4, 5).::(3).::(2).::(1) //same

  println(prependedList2);

  //syntax sugar #4 multi word method naming
  class TeenGirl(name: String){
    def `and then said`(gossip: String) = println(s"$name siad $gossip");
  }

  (new TeenGirl("Lilt")) `and then said` "scary"

  //syntax sugar #5: infix types for two parameters
  class Composite[A, B]
  val composite: Composite[Int, String] = ???
  val composite2: Int Composite String = ???  //WTF

  class --> [A, B]
  val towards: Int --> String = ???

  //syntax sugar #6 update() is very special, like apply
  val anArray = Array(1, 2, 3)
  anArray(2) = 7 //equivalent and rewritten to anArray.update(2, 7)

  //syntax sugar #7 seter for mutable container
  class Mutable{
    private var internalMember: Int = 0
    def member: Int = internalMember //"getter"
    def member_= (value: Int) = internalMember = value //setter
  }

  //now we can write some
  val aMutableContainer = new Mutable
  aMutableContainer.member = 42
}
