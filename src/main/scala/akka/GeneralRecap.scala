package akka

import scala.util.Try

object GeneralRecap extends App {

  def aFunctiono(x: Int) = x + 1

  def anaFun: Int => Int = x => x + 1

  def anaFunShort: Int => Int = _ + 1

  //OOP
  class Animal(val name: String = "")
  class Dog(override val name: String) extends Animal(name)
  val aDog: Animal = new Dog("sparky")

  trait Carnivore{
    def eat(a: Animal) : Unit
  }

  //single abstract method
  val piranah: Carnivore = a => println("I am eating" + a.name);

  piranah.eat(new Dog("poopy"))

  class Crocodile extends Animal with Carnivore{
    override def eat(a: Animal): Unit = println("chomp chomp !!")
  }

  //method notation
  val aCroc = new Crocodile
  aCroc.eat(aDog)
  //equivalent
  aCroc eat aDog

  //generics
  //case classes
  case class Person(name: String, age: Int)

  val aPotentialException = try {
    throw new RuntimeException("lala")
  }catch{
    case e: Exception => println("do some")

  }finally{
    //side effect
  }

  //functional programming
  //function is a special class
  val incrementer = new Function2[Int, Int, Int] {
    override def apply(v1: Int, v2: Int): Int = v1 + v2
  }

  incrementer(10, 20)
  //equivalent to incrementer.apply(10, 20)

  //we can define this as anoamous function
  val incrementerAnon = (x: Int, y: Int) => x + y

  val adder = (x: Int) => x + 1
  //FP is all about working with functions as first class
  //map = HOF, bc it takes another function
  List(1, 2, 3).map(adder)

  //for comprehensions
  val pairs = for {
    num <- List(1, 2, 3, 4)
    char <- List('a', 'b', 'c', 'd')
  } yield num + "-" + char

  //Seq, Array, List, Vector, Map, Tuples, Sets

  val aOption = Some(2)
  val aTry = Try{
    throw new RuntimeException
  }

  //pattern matching
  val unknown = 2
  val order = unknown match{
    case 1 => "first"
    case 2 => "second"
    case _ => "unknown"
  }




}
