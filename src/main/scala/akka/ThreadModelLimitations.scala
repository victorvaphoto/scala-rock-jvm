package akka

import scala.concurrent.Future

object ThreadModelLimitations extends App {

  //synchronization and locking hard on distributed enironment

  //delegating something to thread is pain
  //sending something to a already running thread is hard

  new Thread({ () => print("hello") })

  //typical producer consumer model

  var task: Runnable = null

  var runningThread: Thread = new Thread(() => {
    while (true) {
      while (task == null) {
        runningThread.synchronized(
          {
            println("waiting for a task")
            runningThread.wait() //needs to be notified, wait needs to obtain a lock
          }
        )
      }

      //once gets a task
      task.synchronized({
        println("I have a task")
        task.run()
        task = null
      })
    }
  })

  def delegateToBackgroundThread(r: Runnable) = {
    if (task == null) task = r
    runningThread.synchronized {
      runningThread.notify()
    }
  }

  runningThread.start()
  Thread.sleep(1000)
  delegateToBackgroundThread(() => println(42))
  Thread.sleep(1000)
  delegateToBackgroundThread(() => println("this should run in background"))

  //we need a datastructure
  //that can safely receive messages
  //can identify the server
  //is easily identifiable
  //can guard against errors

  /**
    * DR #3 tracing and dealing with errors is a multi threaded is a pain
    *
    */

  import scala.concurrent.ExecutionContext.Implicits.global

  val futures = (0 to 9).map(i => 10000 until 1000 * (i + 1))
    .map(range => Future({
      if (range.contains(2343)) throw new RuntimeException("poop")
      range.sum
    }))

  val sumFuture = Future.reduceLeft(futures)(_ + _)
  sumFuture.onComplete(println) //this sucks !!!
}
