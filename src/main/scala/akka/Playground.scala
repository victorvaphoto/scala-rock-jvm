package akka

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.util.ByteString

import scala.concurrent._
import scala.concurrent.duration._
import java.nio.file.Paths

import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl.{FileIO, Source}



object Playground extends App {

  //#create-materializer
  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  //#create-materializer

  //#create-source
  val source: Source[Int, NotUsed] = Source(1 to 100)
  //#create-source

  //#run-source
  //val done: Future[Done] = source.runForeach(i ⇒ println(i))(materializer)
  //#run-source

  //BigInt is a starting value
  val factorials = source.scan(BigInt(1))((acc, next) ⇒ acc * next)

//  //#transform-source
//  val result: Future[IOResult] =
//    factorials
//      .map(num ⇒ ByteString(s"$num\n"))
//      .runWith(FileIO.toPath(Paths.get("factorials.txt")))
//  //#transform-source


  //#add-streams
  val done: Future[Done] = factorials
    .zipWith(Source(0 to 100))((num, idx) ⇒ s"$idx! = $num")
    .throttle(1, 1.second)
    //#add-streams
    .take(3)
    //#add-streams
    .runForeach(println)
  //#add-streams

   implicit val ec = system.dispatcher
   done.onComplete(_ ⇒ system.terminate())

}
