package akka

import scala.concurrent.Future

object MultiThreadingRecap extends App{

  val aThread = new Thread(new Runnable {
    override def run(): Unit = println("hello")
  })

  val aThread2 = new Thread(
    () => println("hello there")
  )

  aThread.start();
  aThread2.start();

  val threadHello = new Thread(() => (1 to 1000).foreach(_ => println("hello")))



  class BankAccount(private var amount: Int){
    def withDraw(money: Int) = this.amount -= money

    //this is thread safe
    def safeWithdraw(money: Int) = this.synchronized{
      this.amount -= money
    }
  }

  //alternatively, declare primitive type as volatile
  class BankAccount2(@volatile private var amount: Int){
    def withDraw(money: Int) = this.amount -= money
  }

  //inter-thread communication
  //wait and notify

  //Scala future
  import scala.concurrent.ExecutionContext.Implicits.global
  val future = Future({
    //some long process
    42
  })

  //can run something on complete
  future.onComplete({
    case scala.util.Success(x) => "found"
    case scala.util.Failure(_) => "some happended"
  })

  future.map(_ + 1)
  val aFlatFuture = future.flatMap({ value => Future(value + 2)})

  aFlatFuture.onComplete({
    case scala.util.Success(x) => println("found" + x)
    case scala.util.Failure(_) => println("some happended")
  })

  val filteredFuture = future.filter(_ % 2 == 0)

  val aNonsenseFuture = for{
    meaningOfLife <- future
    filteredMeaning <- filteredFuture
  }yield meaningOfLife + filteredMeaning

  //andThen, recover/recoverWith

  //Promises

  Thread.sleep(1000);


}


