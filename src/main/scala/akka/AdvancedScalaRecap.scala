package akka

import scala.concurrent.Future

class AdvancedScalaRecap extends App{

  //partial functions
  val partialFunction: PartialFunction[Int, Int] = {
    case 1 => 42
    case 2 => 65
    case 5 => 999
  }

  //this is translated as
  val pf = (x: Int) => x match{
    case 1 => 42
    case 2 => 65
    case 5 => 999
  }

  //this is valid, so we can treat partial function as a single param arg function
  val function: (Int => Int) = partialFunction

  List(1, 2, 3, 4).map(partialFunction)

  //or, pass in the partial function as such
  List(1, 2, 3, 4).map(
    {
      case 1 => 42
      case 2 => 65
      case 5 => 999
    }
  )

  //or more succinct, remove the (),
  List(1, 2, 3, 4).map
    {
      case 1 => 42
      case 2 => 65
      case 5 => 999
    }

  //just remember so {} is short hand for ({}), use it for multi line situation, but let's just avoid using this
//
//  List(1, 2, 3, 4).map
//  (
//    case 1 => 42
//    case 2 => 65
//    case 5 => 999
//  )

  //lifting
  val lifted = partialFunction.lift //total function Int => Option[Int]
  lifted(2) //Some(65)
  lifted(5000) //None

  //or Else
  val pfChain = partialFunction.orElse[Int, Int]{
    case 60 => 9000
  }

  pfChain(5) //999 per partial function
  pfChain(60) //9000
  pfChain(457) //throw a match error

  //type aliase
  type ReceiveFunction = PartialFunction[Any, Unit]

  def receive: ReceiveFunction = {
    case 1 => println("hello")
    case _ => println("confused")
  }

  //implicits
  implicit val timeout = 3000
  def setTimeout(f: () => Unit)(implicit timeout: Int) = f()
  setTimeout(() => println("time out")) //extra parameter list omitted

  //implicit conversions
  // 1) implicit defs
  case class Person(name: String){
    def greet = s"Hi, my name is $name"
  }

  implicit def fromStringToPerson(string: String): Person = Person(string)

  "peter".greet //this is done by the compiler to first convert to Person

  //2) implicit class
  implicit class Dog(name: String){
    def bark = println("bark !")
  }

  "Lassie".bark

  //how does compiler organize implicts
  implicit val inverseOrdering: Ordering[Int] = Ordering.fromLessThan(_ > _) //reverse
  List(1, 2, 3).sorted

  //imported scope
  import scala.concurrent.ExecutionContext.Implicits.global
  val future = Future{
    println("hello, future")
  }

  //companion objects of the types included in the call
  object Person{
    implicit val PersonOrdering: Ordering[Person] = Ordering.fromLessThan((a, b) => a.name.compareTo(b.name) < 0)
  }

  List(Person("Bob"), Person("Alice")).sorted

}
