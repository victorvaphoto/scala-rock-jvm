name := "rock-the-jvm-scala"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.21"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.21"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.21" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5"

